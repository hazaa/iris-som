# IRIS-SOM

Utilisation de données relatives aux IRIS pour créer une carte auto-organisée (SOM).

*   [Publications](https://gitlab.com/hazaa/iris-som#publications)
*   [Installation](https://gitlab.com/hazaa/iris-som#installation)
*   [Ségrégation: quelques résultats](https://gitlab.com/hazaa/iris-som#s%C3%A9gr%C3%A9gation-quelques-r%C3%A9sultats)
*   [Distance géographique vs distance SOM](https://gitlab.com/hazaa/iris-som#distance-g%C3%A9ographique-vs-distance-som)
*   [Distance géographique vs distance SOM avec boxplot](https://gitlab.com/hazaa/iris-som#distance-g%C3%A9ographique-vs-distance-som-avec-boxplot)
*   [Volages](https://gitlab.com/hazaa/iris-som#volages-robustesse) 

# Publications

*   "Multidimensional segregation: an exploratory case study" [arXiv](https://arxiv.org/abs/1705.03213)

# Installation

Les fichiers R dépendent des paquets suivants:
1.  paquets debian: libgdal-dev libproj-dev, libxml2-dev, r-cran-slam
1.  paquets R: rgdal, RSQLite, sp, seg, SOMbrero


Pour cloner ce dépôt sur votre machine, il faut installer git (sudo apt-get install git), puis:

    git clone gitlab.com:hazaa/iris-som.git
    

# Description des fichiers

*  contour_iris_examples.R:   affiche les polygones correspondant aux contours des iris. 
*  RP_examples.R: lit la base sqlite data/FD_LOGEMTZA_2012.db, au format [sqlite](http://sqlite.org/). 
*  revenu_examples.R: lit les revenus dans le fichier data/FiloSoFI_Paris.csv, et les joint à celles du recensement. 
*  voir le contenu du répertoire [data](https://gitlab.com/hazaa/iris-som/edit/master/data)
*  aggregate_RP_hlm.py: crée une table supplémentaire dans la base sqlite data/FD_LOGEMTZA_2012.db, contenant pour chaque iris de Paris le nombre de logements échantillonnés et le nombre de logements hlm parmi ceux-ci. 
*  aggregate_RP.py: crée une table supplémentaire dans la base sqlite data/FD_LOGEMTZA_2012.db, contenant pour chaque iris de Paris la moyenne de agemen8 et de inp18m  
*  RP_check_missing.R: compte le pourcentage de données manquantes par iris. Un résumé est écrit dans data/missing_data.txt

# Exemple d'utilisation

Lecture des données logement agrégées sous R:
    
    >source('RP_examples.R')
    >df.rp
    
Affichage de fonds de cartes sous R:     
    
    >source('contour_iris_examples.R')
    
Jointure revenu/recensement:

    >source('revenu_examples.R')   
    

# Obtenir les données

 * Fichier Logement, Recensement de la Population Ile de France: [dictionnaire des variables](https://www.insee.fr/fr/statistiques/1913207?sommaire=1912584#dictionnaire)
    *  2013: pas disponible
    *  2012: [insee](http://www.insee.fr/fr/themes/detail.asp?ref_id=fd-rp2012&page=fichiers_detail/rp2012/telechargement.htm), fichier [RP2012_LOGEMTza.txt](http://telechargement.insee.fr/fichiersdetail/RP2012/txt/RP2012_LOGEMTza_txt.zip)
 * Revenus fiscaux déclarés localisés:  
    * 2012: [insee](http://www.insee.fr/fr/themes/detail.asp?reg_id=0&ref_id=infra), format xls
 * Contours des Iris: https://www.data.gouv.fr/fr/datasets/contours-iris/


Ségrégation: préliminaires
======

L'indice $`H^R`$ de Reardon, aussi nommé "spatial information theory index",
Il vise à distinguer les situations qui ne sont pas séparées par les autres 
indices de ségrégation spatiale (comme rappelé par [Dabet-Floch 2014](https://www.insee.fr/fr/statistiques/1381158) "1.2 Indicateurs spatiaux et aspatiaux", en citant Feitosa 04):

![feitosa_04](fig/feitosa_04.png)

Pour Dabet et Floch:


	"Si l’on calcule le classique indicateur d’entropie de Theil pour chacune des trois
	configurations, on trouve la même valeur, malgré les évidentes différences morphologiques.
	La configuration A montre une forte ségrégation à grande comme à petite échelle. La
	C, l’absence complète de ségrégation. La B est intermédiaire puisqu’il y a ségrégation à
	petite échelle, celle-ci ne se manifestant pas à l’échelle de l’ensemble du territoire. Mais les
	indices d’entropie calculés au niveau du carreau, et qui prennent la valeur 0 ne permettent
	pas de distinguer les situations A et B."
	

Soit dd un tableau où les lignes sont les iris, les colonnes sont les classes.
dd[i,j] est le nombre d'invidus de classe j dans l'iris i.
$`H^R`$ est calculé comme suit dans le package seg:

	
	# cf spatseg.R, localenv.R
	ee : defini par interpolation spatiale a partir de dd, et des coordonnees des polygones 
    m <- ncol(dd) # Number of population groups 
    ptsSum <- sum(dd) # Total population in the study area
    ptsRowSum <- apply(dd, 1, sum) # Population of all groups at each data point
    ptsColSum <- apply(dd, 2, sum) # Total population of each subgroup
    ptsProp <- ptsColSum / ptsSum # Proportion of each subgroup
    envProp <- t(apply(ee, 1, function(z) z/sum(z))) # Population proportion of each group at each local environment
	Ep <- apply(envProp, 1, function(z) sum(z * log(z, base = m))) * -1
	E <- sum(ptsProp * log(ptsProp, base = m)) * -1
	H <- 1 - (sum(ptsRowSum * Ep) / (ptsSum * E))


On voit que:
 *   on calcule dans chaque iris un indice d'entropie ("Ep <-..."). La normalisation se fait par le dénominateur, qui reflète l'entropie globale de la population.
 *   le caractère spatial des données n'est pas utilisé dans cette formule. Pour le prendre en compte, le package seg  prétraite le tableau dd en effectuant un smoothing, ce qui donne le tableau ee. C'est lors de cette étape que la proximité géographique entre iris est utilisée. Plusieurs méthodes sont employées, telles qu'un kernel (voir des exemples dans [segregation_examples.md](segregation_examples.md)).
 *   la méthode "geo" que nous avons utilisée ci-dessous revient à faire du smoothing.
 

Des tests sur données synthétiques avec 2 classes sont discutés dans [segregation_examples.md](segregation_examples.md).


Ségrégation: quelques résultats
======


Pour calculer les indices de ségrégation dans le package R [seg](https://cran.r-project.org/web/packages/seg/index.html), il faut fournir un
décompte du nombre d'individus de chaque classe, pour chaque iris de la ville.


Or nous ne disposons que d'un label par iris, suite à la classification. 
Dans segregation_som.R, nous pré-traitons les données, en utilisant plusieurs approches:

 * geo: décompte local des classes auxquelles appartiennent les voisins géographiques. Le paramètre est le nombre de voisins, knn 
 * alpha: si l'iris i est affecté à la classe 1 (parmi 4), alors nous lui affectons le vecteur (alpha,(1-alpha)/3,(1-alpha)/3,(1-alpha)/3 ). Nous testons deux valeurs alpha=0.5, ou 0.75
 * som: chaque iris est associé à un prototype. Ce prototype a des voisins. Ces voisins sont associés à des iris. Ces iris sont associés à des superclasses. Pour chaque iris, on peut donc associer un décompte des superclasses associées aux voisins de son prototype. Problème: certains prototypes ont 5 voisins (lui-même, haut, bas, gauche, droite). D'autres situés dans les coins de la carte n'ont que 3 voisins. On doit donc normaliser au niveau de chaque iris le décompte obtenu.
 * smooth: on utilise la méthode alpha, avec le paramètre 1.0 (donc chaque iris a une description binaire [100 0 0 0]). Puis on calcule l'indice de Reardon avec un lissage spatial (smooth), qui transforme notre carte en une image composée de nr x nc pixels . Il y a plusieurs paramètres à régler: la taille de l'image, et le paramètre de lissage (la largeur du noyau). Dans les exemples ci-dessous, ce paramètre est choisi automatiquement. 
 
 
De plus, nous testons l'indicateur de ségrégation sur des données synthétiques:
 
 * unif_seg_1:  la composition de tous les iris est (10, 16, 27, 45). Comme unif_seg_2, mais avec une distribution un peu moins concentrée. 
 * unif_seg_2: la composition de tous les iris est la même (3, 8, 23, 64).  C'est, en arrondissant et en normalisant à 100, l'exponentielle du vecteur [1,2,3,4], qui nous donne une distribution très concentrée en sous-population de classe 4.Le but est de voir que même si au sein de chaque iris, la répartition en sous-groupes est très hétérogène, on obtient une mesure très faible de ségrégation au niveau de la ville, car la répartition ci-dessus est la même dans tous les iris. 
 * mixed_seg: un iris sur deux a la composition  (64, 23, 8, 3); et l'autre la composition (3, 8, 23, 64). Contrairement à ci-dessus, tous les iris n'ont pas la même composition. La moitié des iris sur-représente la classe 1, et l'autre moitié sur-représente la classe 4; pour imiter un damier. On le voit bien sur l'image synth_mixed_seg en comparant les cadrans 1 et 4 qui sont un les négatifs l'un de l'autre 
 * random: tous les iris ont une composition aléatoire, de somme constante.
 
## Valeurs des indicateurs

| label | indice de ségrégation H |  Commentaire  |
| -------- | -------- | -------- |
| som1 s_geo(25)    | 0.12    |   |
| som1 s_geo(10)    | 0.23    |   |
| som1 s_alpha(0.5)   | 0.09|    |
| som1 s_alpha(0.75) | 0.36     |   |
| som1 s_som| 0.05 ||
| som1 s_smooth| 0.18 | valeur située entre som1 s_geo(25) et som1 s_geo(10)|
| som2 s_geo(25) | 0.10 | |
| som2 s_geo(10) | 0.21 | |
| som2 s_alpha(0.5) |0.08 | |
| som2 s_alpha(0.75) | 0.28 | |
| som3 s_geo(25) | 0.14 | 6 superclasses (au lieu de 4 pour som1 et som2) |
| som3 s_geo(10) | 0.27 | 6 superclasses |
| synth mixed_seg | 0.28 | |
| synth unif_seg_1 | -9.8e-15 || 
| synth unif_seg_2 | 9.8e-15 || 
| synth random | 0.12||


## Composition de chaque iris, par cluster 

Les figures se composent de 4 cadrans pour som 1 et som 2, et de 6 cadrans pour som 3 (autant que de sous-classes dans chaque iris). Chaque cadran concerne une des sous-classes uniquement, et
la couleur de chaque iris dans ce cadran reflète la plus ou moins forte
teneur donne de ce sous-groupe dans cet iris. 

| label | carte des clusters | 
| -------- | -------- | 
|som1 geo(25)| ![lc_geo_som1_25](fig/lc_geo_som1_25.png) |
|som1 geo(10)| ![lc_geo_som1_10](fig/lc_geo_som1_10.png) |
|som2 geo(10)| ![lc_geo_som2_10](fig/lc_geo_som2_10.png) |
|som3 geo(10)| ![lc_geo_som3_10](fig/lc_geo_som3_10.png) |
|som1 alpha(0.5) | ![lc_alpha_som1](fig/lc_alpha_som1.png) |
|som1 som  | ![lc_som_som1](fig/lc_som_som1.png) |
|som1 smooth  | ![lc_smooth_som1](fig/lc_smooth_som1.png) |
|synth_mixed_seg | ![lc_synth_mixed_seg](fig/lc_synth_mixed_seg.png) |
|synth_unif_seg_1 | ![lc_synth_unif_seg_1](fig/lc_synth_unif_seg_1.png)|
|synth_unif_seg_2 | ![lc_synth_unif_seg_2](fig/lc_synth_unif_seg_2.png) |
|synth_random | ![lc_synth_random](fig/lc_synth_random.png)|

# Distance géographique vs distance SOM

voir le code [distdist.R](distdist.R) 

| som | plot |  type distance (geo, som)| comment |
| -------- | -------- | -------- | -------- |
| 1 | ![distdist_1](fig/distdist_1.png) | eucl,max | |
| 1 | ![distdist_histogram](fig/distdist_hist.png) | eucl,max | |
| 1 | ![distdist_histogram_rot](fig/distdist_hist_rotate.png) | eucl,max |  rotated |
| 1 | ![distdist_histogram_norm_1](fig/distdist_hist_normalized_1.png) | eucl,max |  normalized |
| 1 | ![distdist_histogram_conditional](fig/distdist_hist_conditional.png) | eucl,max |  conditional, not normalized|
| 2 | ![distdist_histogram_norm_2](fig/distdist_hist_normalized_2.png) | eucl,max |  normalized |
| 2 | ![distdist_box_2](fig/distdist_box_2.png) | eucl,max |   |
| 3 | ![distdist_histogram_norm_3](fig/distdist_hist_normalized_3.png) | eucl,max |  normalized |
| 3 | ![distdist_box_3](fig/distdist_box_3.png) | eucl,max |   |


# Distance géographique vs distance SOM avec boxplot

voir le code [distdist_compare.R](distdist_compare.R) 

| label | carte des clusters | distdist | type distance (geo, som)| algo clustering |
| -------- | -------- | -------- | -------- | -------- |
|synth_random | ![lc_synth_random](fig/lc_synth_random.png)| ![distdist_box_synth_random](fig/distdist_box_synth_random.png) |  eucl,ficelle | oracle |
|synth_unif_seg_1 | ![lc_synth_unif_seg_1](fig/lc_synth_unif_seg_1.png)| ![distdist_box_synth_unif_seg_1](fig/distdist_box_synth_unif_seg_1.png) | eucl,ficelle | oracle |
|synth_mixed_seg| ![lc_synth_mixed_seg](fig/lc_synth_mixed_seg.png)| ![distdist_box_synth_mixed_seg](fig/distdist_box_synth_mixed_seg.png) | eucl,ficelle | oracle |
|feitosa_ora_1| ![lc_feitosa_1](fig/lc_feitosa_1.png)| ![distdist_box_feitosa_1](fig/distdist_box_feitosa_1.png) | eucl,max |  oracle|
|feitosa_ora_2| ![lc_feitosa_2](fig/lc_feitosa_2.png)| ![distdist_box_feitosa_2](fig/distdist_box_feitosa_2.png) | eucl,max |  oracle|
|feitosa_ora_3| ![lc_feitosa_3](fig/lc_feitosa_3.png)| ![distdist_box_feitosa_3](fig/distdist_box_feitosa_3.png) | eucl,max |  oracle|
|feitosa_som_1| cf ci-dessus | ![distdist_box_feitosa_som1](fig/distdist_box_feitosa_som1.png) | eucl,max |  som|
|feitosa_som_2| cf ci-dessus | ![distdist_box_feitosa_som2](fig/distdist_box_feitosa_som2.png) | eucl,max |  som|
|feitosa_som_3| cf ci-dessus | echec trainSOM | eucl,max |  som|


Pour feitosa_som_x avec du bruit gaussien sur la composition:

>    segdata[k,]<- prob + 25*noise*rnorm(4) 

* x=1: prob=(100,0,0,0) pour le quadrant 1, (0,100,0,0) pour le quadrant 2, etc...  
* x=2: prob=(100,0,0,0) pour le "sous-quadrant" 1, (0,100,0,0) pour le "sous-quadrant" 2, etc...   
* x=3: prob=(25,25,25,25) pour toutes les cases.

| id |niveau bruit |  distdist | carte som | carte des clusters |
| --------| -------- | -------- | -------- | -------- |
|1| 0.01 | ![distdist_box_feitosa_som1_noise0.01](fig/distdist_box_feitosa_som1_noise0.01.png) | ![grid_feitosa_som1_noise0.01](fig/grid_feitosa_som1_noise0.01.png) | RAS (pas assez de bruit) |
|1| 0.05 | ![distdist_box_feitosa_som1_noise0.05](fig/distdist_box_feitosa_som1_noise0.05.png) | ![grid_feitosa_som1_noise0.05](fig/grid_feitosa_som1_noise0.05.png) | RAS (pas assez de bruit) |
|1| 0.1 | ![distdist_box_feitosa_som1_noise0.1](fig/distdist_box_feitosa_som1_noise0.1.png) | ![grid_feitosa_som1_noise0.1](fig/grid_feitosa_som1_noise0.1.png) | ![lc_feitosa_1_noise0.1](fig/lc_feitosa_1_noise0.1.png)| 
|1| 1.0 | ![distdist_box_feitosa_som1_noise1](fig/distdist_box_feitosa_som1_noise1.png) | ![grid_feitosa_som1_noise1](fig/grid_feitosa_som1_noise1.png) | ![lc_feitosa_1_noise1](fig/lc_feitosa_1_noise1.png)| 
|1| 2.0 | ![distdist_box_feitosa_som1_noise2](fig/distdist_box_feitosa_som1_noise2.png) | ![grid_feitosa_som1_noise2](fig/grid_feitosa_som1_noise2.png) | ![lc_feitosa_1_noise2](fig/lc_feitosa_1_noise2.png)| 
|2| 0.01 | ![distdist_box_feitosa_som2_noise0.01](fig/distdist_box_feitosa_som2_noise0.01.png) | ![grid_feitosa_som2_noise0.01](fig/grid_feitosa_som2_noise0.01.png) | RAS (pas assez de bruit) |
|2| 0.05 | ![distdist_box_feitosa_som2_noise0.05](fig/distdist_box_feitosa_som2_noise0.05.png) | ![grid_feitosa_som2_noise0.05](fig/grid_feitosa_som2_noise0.05.png) | RAS (pas assez de bruit) |
|2| 0.1 | ![distdist_box_feitosa_som2_noise0.1](fig/distdist_box_feitosa_som2_noise0.1.png) | ![grid_feitosa_som2_noise0.1](fig/grid_feitosa_som2_noise0.1.png) | ![lc_feitosa_2_noise0.1](fig/lc_feitosa_2_noise0.1.png)| 
|2| 1.0 | ![distdist_box_feitosa_som2_noise1](fig/distdist_box_feitosa_som2_noise1.png) | ![grid_feitosa_som2_noise1](fig/grid_feitosa_som2_noise1.png) | ![lc_feitosa_2_noise1](fig/lc_feitosa_2_noise1.png)| 
|2| 2.0 | ![distdist_box_feitosa_som2_noise2](fig/distdist_box_feitosa_som2_noise2.png) | ![grid_feitosa_som2_noise2](fig/grid_feitosa_som2_noise2.png) | ![lc_feitosa_2_noise2](fig/lc_feitosa_2_noise2.png)| 
|3| 0.01 | ![distdist_box_feitosa_som3_noise0.01](fig/distdist_box_feitosa_som3_noise0.01.png) | ![grid_feitosa_som3_noise0.01](fig/grid_feitosa_som3_noise0.01.png) | pas fait |
|3| 0.05 | ![distdist_box_feitosa_som3_noise0.05](fig/distdist_box_feitosa_som3_noise0.05.png) | ![grid_feitosa_som3_noise0.05](fig/grid_feitosa_som3_noise0.05.png) | pas fait |
|3| 0.1 | ![distdist_box_feitosa_som3_noise0.1](fig/distdist_box_feitosa_som3_noise0.1.png) | ![grid_feitosa_som3_noise0.1](fig/grid_feitosa_som3_noise0.1.png) | ![lc_feitosa_3_noise0.1](fig/lc_feitosa_3_noise0.1.png)|  

# Volages - Robustesse

Voir le code [Fickle-iris-R.R](Fickle-iris-R.R).

Pour chaque paire d'iris, on regarde s'il s'agit plutôt d'une "attraction" (toujours voisins), 
d'une "répulsion" (jamais voisins) ou d'une paire "volage" (pour plus d'info voir l'article [Neurocomputing 14](https://hal.archives-ouvertes.fr/hal-01168120v1) de Marie Cottrell avec Nicolas Bourgeois et Stéphane Lamassé). On peut ensuite calculer les iris les plus "volages", etc... 

*   Pour le premier groupe (revenus, part de patrimoine, etc...) on a : 23.52% de paires "attracting", 72.38% de "repelling" et 4.10% de "fickle"
*   Pour le deuxième (population) : 23.90%, 69.25%, 6.85%
*   Pour les services, etc: 24.04%, 66.39%, 9.57%


Les 100 plus "fickles" et les 100 moins "fickle", par groupe de variable et par arrondissement:

Sur les revenus, les plus volages:

    >75101 75103 75104 75105 75106 75109 75110 75111 75112 75113 75114 75115 75116 75117 75118 75119 75120
    >1     2     3     1     2     2     4    17     3     8     8    11     5     6    10    11   6

les moins volages  :

    >75101 75104 75105 75106 75107 75108 75111 75113 75114 75115 75116 75117 75118 75119 75120 
    >1     1     1     7    16     3     1     4     2     2    35     8     6     6     7 

Sur les variables de population, les plus volages:
 
    >75101 75102 75103 75104 75106 75107 75108 75109 75110 75111 75112 75113 75114 75115 75116 75117 75118 75119 75120
    >6     1     3    11     3     2     4     9     6     6    10     4     6     9     3     6   4     3     4     
 
les moins volages :
 
    >75101 75102 75104 75105 75106 75107 75108 75110 75111 75112 75113 75114 75115 75116 75117 75118 75119 75120
    >1     2     1     2     3     4    10     5     1     3    12     9     3     4     6     7  18     9  

Sur les services, les plus volages :

    > summary(as.factor(arr.vect[1:100]))
    >75101 75103 75105 75110 75111 75112 75113 75114 75115 75116 75117 75118 75119 75120 
    >1     2     5     2    15     8    10     9     7     3     9    19     7     3 
 
les moins volages : 
 
    >summary(as.factor(arr.vect[881:980]))
    >75101 75102 75103 75106 75108 75109 75110 75115 75118 75119 75120 
    >10    13     2     4    17    11     8     5    11    13     6  
 
 

| label | image  |
| -------- | -------- |
| volages_SOM1 | ![volages_SOM1](fig/volages_SOM1.png) |
| volages_SOM2| ![volages_SOM2](fig/volages_SOM2.png) |
| volages_SOM3| ![volages_SOM3](fig/volages_SOM3.png) |
| volages_Fickleness-SOM| ![volages_Fickleness-SOM](fig/volages_Fickleness-SOM.png) |
| volages_feitosa_noise=0.1 | ![volages_feitosa_noise0.1](fig/volages_feitosa_noise0.1.png)_|
| volages_feitosa_noise=1.0 | ![volages_feitosa_noise1](fig/volages_feitosa_noise1.png)_|


# TODO


 * [KO!] faire boxplot sur som 2,3 => le fichier.R qui a servi pour créer fig/Paris1SomIBox.pdf n'est pas dispo
 * [KO!] fig 11: refaire en plus clair, mieux séparer les images, bruiter.
 * [OK] fig 13: la créer
 * [OK] fig 12: bruiter feitosa 1,2; ajouter les carte de Kohonen correspondantes
 * [OK] fig 10: refaire en supprimant le titre et le sous-titre 
 * [OK] feitosa3 + som + noise
 * [OK] remplacement de INP24M par INP18M
 * [OK] décompte du nombre de foyers tels que emplm=zz
 * [OK] DIPLM: regroupement des classes (DIPLM-> DIPLM_recoded)
 * [OK] calcul de moyenne variance  pour INP18M, AGEMEN8,DIPLM_recoded
 * [] données manquantes
 * [] db: reprendre structure de la base (e.g. avec pandas et MultiIndex). Les iris_id doivent être stockées dans une seule base, et pas dupliquées. 
 * [] unit test 
 * [] biblio https://www.insee.fr/fr/statistiques/2408659, https://www.insee.fr/fr/statistiques/2017608?sommaire=2017614&q=dabet+floch
 * [] integrer les flux de données: pas de fichier intermédiaire comme IRIS_IGN.csv, garder l'identifiant d'iris dans som*.rda
 * [] unit test
 * [] feitosa 1,2 + som ficelle (au lieu de ora)
 * [] visualisation holoviews
 * [] wavelet 
 * [] cécile
