# -*- coding: utf-8

#  aggregate_RP.py
#  
#  Copyright 2016 Aurélien Hazan <aurelien.hazan@u-pec.fr>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  


"""
Create 1 new table in RP with the average and std of: AGEMEN8, INP18N, DIPLM_recoded,
by iris.

LEFT JOIN mauy be needed, because all iris need to be present in the hlm
table, even if no data correspond

https://docs.python.org/2.7/library/sqlite3.html
https://en.wikibooks.org/wiki/Structured_Query_Language
https://en.wikibooks.org/wiki/Structured_Query_Language/SELECT:_Join_Operation#Left_.28outer.29_Join

std is not a built-in function in sqlite3
=> user-defined aggregator function are needed:
http://stackoverflow.com/questions/2298339/standard-deviation-for-sqlite
"""

import sqlite3
import sqlite_func
import numpy as np
import os


# OPEN DB
fname_rp = 'data/FD_LOGEMTZA_2012.db'

# temporary dir 
# http://stackoverflow.com/questions/10394517/setting-sqlite-temp-store-directory
temp_dir = '/home/aurelien/local/data/tmp' # seems necessary to use large disk space for the join below
os.environ['SQLITE_TMPDIR'] = temp_dir
print "setting environment variable SQLITE_TMPDIR=",temp_dir
conn = sqlite3.connect(fname_rp)

# user-defined aggregator function:
conn.create_aggregate("stdev", 1, sqlite_func.StdevFunc0)
c = conn.cursor()

# create temp tables
print "create tables ..."
r="""CREATE TABLE avg_std( iris_id integer, avg_agemen8 real, std_agemen8 real,
											avg_inp18m real, std_inp18m real,
											avg_diplm real, std_diplm real
											 );
     CREATE TABLE tmp( iris_id integer, agemen8 integer, inp18m integer, diplm_recoded integer );""" 											 
c.executescript(r)


# full join, averaging agemen8
# http://sqlite.org/lang_aggfunc.html#avg
# "The avg() function returns the average value of all non-NULL X within a group. String and BLOB values that do not look like numbers are interpreted as 0."
# NB: this works because b.agemen8=='YY' occurs exactly when b.inp18m=='Y'	
print "join tables iris_unique_id, logement_2012, diplm_recoded... "
r_full="""
  INSERT INTO tmp(iris_id, agemen8, inp18m, diplm_recoded)
     SELECT a.iris_id, b.agemen8, b.inp18m, c.diplm_recoded    
     FROM iris_unique_id AS a 
     LEFT OUTER JOIN  logement_2012 AS b ON b.IRIS == a.iris_id
     LEFT OUTER JOIN  diplm_recoded AS c ON c.iris_id == a.iris_id
     WHERE b.agemen8!='YY'; 
     """
r="""
  INSERT INTO tmp(iris_id, agemen8, inp18m, diplm_recoded)
     SELECT a.iris_id, b.agemen8, b.inp18m, c.diplm_recoded    
     FROM iris_unique_id AS a 
     JOIN  logement_2012 AS b ON b.IRIS == a.iris_id
     JOIN  diplm_recoded AS c ON c.iris_id == a.iris_id
     WHERE b.agemen8!='YY'; 
     """     
c.execute(r)
print "averaging/stding: agemen8, inp18m, diplm_recoded"
r="""     
   INSERT INTO avg_std(iris_id, avg_agemen8, std_agemen8,avg_inp18m,  std_inp18m, 
						avg_diplm, std_diplm )
     SELECT iris_id, avg(agemen8), stdev(agemen8), avg(inp18m), stdev(inp18m),avg(diplm_recoded), stdev(diplm_recoded)
     FROM tmp
     GROUP BY iris_id
     ORDER BY iris_id    ;      
     """
c.execute(r)

# clean temporary tables
print "dropping temporary tables..."
r="""drop table tmp;"""
c.execute(r)

# vérification: std:avec agemen8
#    compute std with numpy
r="""SELECT b.agemen8 FROM iris_unique_id AS a 
     LEFT OUTER JOIN  logement_2012 AS b ON b.IRIS == a.iris_id
     WHERE b.agemen8!='YY' AND a.iris_id=='751208026';     
     """
c.execute(r)
records = c.fetchall()
a=np.array(records,dtype=float)
std_np = np.std(a)
#    compute with sqlite aggregate function
r="""SELECT stdev(b.agemen8) FROM iris_unique_id AS a 
     LEFT OUTER JOIN  logement_2012 AS b ON b.IRIS == a.iris_id
     WHERE b.agemen8!='YY' AND a.iris_id=='751208026';     
     """
c.execute(r)
std_sqlite= c.fetchone()[0]
print "std_np=",std_np," std_sqlite=",std_sqlite
# vérification avec inp18m
# ?????????????????????
# ?????????????????????

r="""vacuum;"""
c.execute(r)

conn.commit()
conn.close()
