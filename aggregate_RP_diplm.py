# -*- coding: utf-8

#  aggregate_RP_diplm.py
#  
#  Copyright 2016 Aurélien Hazan <aurelien.hazan@u-pec.fr>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

"""
recode in sqlite3 ?
in R: http://rprogramming.net/recode-data-in-r/

liste des variables (Logements ordinaires, fichier détail):
2012: http://www.insee.fr/fr/statistiques/1913207?sommaire=1912584#dictionnaire
2013: https://www.insee.fr/fr/statistiques/2409491?sommaire=2409559#dictionnaire

DIPLM_15
A : Aucun diplôme ou au mieux BEPC, brevet des collèges, DNB
B : CAP, BEP
C : Baccalauréat (général, technologique, professionnel)
D : Diplôme d'études supérieures
Z : Hors champ (moins de 14 ans)
YY : Hors résidence principale

DIPLM:
01 : Pas de scolarité
02 : Aucun diplôme mais scolarité jusqu'en école primaire ou au collège
03 : Aucun diplôme mais scolarité au delà du collège
11 : Certificat d'études primaires
12 : BEPC, brevet élémentaire, brevet des collèges
13 : Certificat d'aptitudes professionnelles, brevet de compagnon
14 : Brevet d'études professionnelles
15 : Baccalauréat général, brevet supérieur
16 : Bac technologique ou professionnel, brevet
professionnel ou de technicien, BEC, BEI, BEH, capacité en
droit
17 : Diplôme universitaire de 1er cycle, BTS, DUT, diplôme
des professions sociales ou de santé, d'infirmier(ère)
18 : Diplôme universitaire de 2ème ou 3ème cycle (y
compris médecine, pharmacie, dentaire), diplôme
d'ingénieur, d'une grande école, doctorat, etc.
YY : Hors résidence principale
ZZ : Personne âgée de moins de 14 ans

RECODAGE DIPLM->DIPLM_15
A<-01, 02, 03, 11, 12
B<-13,14
C<-15,16
D<-17,18
YY<-YY
ZZ<-ZZ

RECODAGE iris-som:
1<-01, 02, 03, 11, 12
2<-13,14
3<-15,16
4<-17,18
YY<-YY
ZZ<-ZZ

"""

import sqlite3

# OPEN DB
fname_rp = 'data/FD_LOGEMTZA_2012.db'
conn = sqlite3.connect(fname_rp)
c = conn.cursor()

# create temp tables
print "create tables ..."
r="""CREATE TABLE diplm_recoded( iris_id integer, diplm_recoded integer );"""
c.execute(r)

# copy diplm
print "copy diplm..."
r="""INSERT INTO diplm_recoded SELECT iris,diplm FROM logement_2012;	"""
c.execute(r)

# recoding
# UPDATE/SET: http://stackoverflow.com/questions/4968841/case-statement-in-sqlite-query#4968918
print "recoding ..."
r="""UPDATE diplm_recoded SET diplm_recoded = 1 WHERE diplm_recoded == 1 OR diplm_recoded == 2 OR diplm_recoded == 3 OR diplm_recoded == 11 OR diplm_recoded == 12 ;
	UPDATE diplm_recoded SET diplm_recoded = 2 WHERE diplm_recoded == 13 OR diplm_recoded == 14;
	UPDATE diplm_recoded SET diplm_recoded = 3 WHERE diplm_recoded == 15 OR diplm_recoded == 16;
	UPDATE diplm_recoded SET diplm_recoded = 4 WHERE diplm_recoded == 17 OR diplm_recoded == 18;
	"""
c.executescript(r)


"""
TEST

create table test(i integer);
insert into test(i) values (1);
insert into test(i) values (2);
insert into test(i) values (3);
UPDATE test SET i = CASE i WHEN i >= 2 THEN 0 	ELSE i   END;
select * from test;        

create table test2(i integer);
INSERT INTO test2 SELECT * FROM test;		
UPDATE test2 SET i = 0 WHERE i == 1 OR i == 2;
select * from test2;        

"""

conn.commit()
conn.close()
