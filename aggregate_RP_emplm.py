# -*- coding: utf-8

#  aggregate_RP_emplm.py
#  
#  Copyright 2017 Aurélien Hazan <aurelien.hazan@u-pec.fr>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  


"""
Create a new table in RP with unique iris identifiers
Create a new table in RP with the aggregate number of emplm='ZZ' by iris

NB: we need LEFT JOIN, because all iris need to be present in the hlm
table, even if no data correspond

https://docs.python.org/2.7/library/sqlite3.html
https://en.wikibooks.org/wiki/Structured_Query_Language
https://en.wikibooks.org/wiki/Structured_Query_Language/SELECT:_Join_Operation#Left_.28outer.29_Join
"""

import sqlite3

# OPEN DB
fname_rp = 'data/FD_LOGEMTZA_2012.db'  

conn = sqlite3.connect(fname_rp)
c = conn.cursor()

# create temp tables
print "create temp tables ..."
r="""CREATE TABLE emplm_ZZ( iris_id integer, nb_logement integer, nb_logement_emplm_ZZ integer);"""
c.execute(r)
r="""CREATE TABLE tmp_emplm_ZZ( iris_id integer, nb_logement_emplm_ZZ integer);"""
c.execute(r)
r="""CREATE TABLE tmp_nb( iris_id integer, nb_logement integer);"""
c.execute(r)

# full join 
print "full join "
r="""
  INSERT INTO tmp_nb(iris_id, nb_logement)
     SELECT a.iris_id,count(*) FROM iris_unique_id AS a 
     LEFT OUTER JOIN  logement_2012 AS b ON b.IRIS == a.iris_id
     GROUP BY a.iris_id;
     """
c.execute(r)

# join where emplm=ZZ
# ZZ : chômage
print "join with emplm=ZZ..."

r="""
  INSERT INTO tmp_emplm_ZZ(iris_id, nb_logement_emplm_ZZ )
     SELECT a.iris_id, count(*) FROM iris_unique_id AS a 
     LEFT OUTER JOIN  logement_2012 AS b ON b.IRIS == a.iris_id
     WHERE b.emplm = 'ZZ' 
     GROUP BY a.iris_id;
     """
c.execute(r)

# final join and insert
print "final insert..."
r="""
INSERT INTO emplm_ZZ( iris_id, nb_logement, nb_logement_emplm_ZZ)
     SELECT tmp_nb.iris_id, tmp_nb.nb_logement, tmp_emplm_ZZ.nb_logement_emplm_ZZ 
     FROM tmp_nb
     LEFT OUTER JOIN tmp_emplm_ZZ ON tmp_nb.iris_id==tmp_emplm_ZZ.iris_id
     ORDER BY tmp_nb.iris_id;
"""
c.execute(r)
 
# drop temp tables, closing
r="""DROP TABLE tmp_nb;"""
c.execute(r)
r="""DROP TABLE tmp_emplm_ZZ;"""
c.execute(r)

conn.commit()
conn.close()
