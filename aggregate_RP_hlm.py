# -*- coding: utf-8

#  aggregate_RP_hlm.py
#  
#  Copyright 2016 Aurélien Hazan <aurelien.hazan@u-pec.fr>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  


"""
Create a new table in RP with unique iris identifiers
Create a new table in RP with the aggregate number of hlm by iris

NB: we need LEFT JOIN, because all iris need to be present in the hlm
table, even if no data correspond

https://docs.python.org/2.7/library/sqlite3.html
https://en.wikibooks.org/wiki/Structured_Query_Language
https://en.wikibooks.org/wiki/Structured_Query_Language/SELECT:_Join_Operation#Left_.28outer.29_Join
"""

import sqlite3

# OPEN DB
fname_rp = 'data/FD_LOGEMTZA_2012.db'
conn = sqlite3.connect(fname_rp)
c = conn.cursor()

# create new table to record the unique ids of iris
r="""CREATE TABLE iris_unique_id( iris_id integer);"""
c.execute(r)
r="""INSERT INTO iris_unique_id(iris_id)
	 SELECT DISTINCT iris from logement_2012;
	"""
c.execute(r)

# checksum
r="""SELECT count(*) from (SELECT DISTINCT iris FROM logement_2012); """
c.execute(r)

# create temp tables
print "create temp tables ..."
r="""CREATE TABLE hlm( iris_id integer, nb_logement integer, nb_logement_hlm integer);"""
c.execute(r)
r="""CREATE TABLE tmp_nb( iris_id integer, nb_logement integer);"""
c.execute(r)
r="""CREATE TABLE tmp_nb_hlm( iris_id integer, nb_logement_hlm integer);"""
c.execute(r)

# full join 
print "full join "
r="""
  INSERT INTO tmp_nb(iris_id, nb_logement)
     SELECT a.iris_id,count(*) FROM iris_unique_id AS a 
     LEFT OUTER JOIN  logement_2012 AS b ON b.IRIS == a.iris_id
     GROUP BY a.iris_id;
     """
c.execute(r)

# join where hlm=1
# 1 : Logement appartenant à un organisme HLM
print "join with hlm=1 ..."

r="""
  INSERT INTO tmp_nb_hlm(iris_id, nb_logement_hlm)
     SELECT a.iris_id, count(*) FROM iris_unique_id AS a 
     LEFT OUTER JOIN  logement_2012 AS b ON b.IRIS == a.iris_id
     WHERE b.HLML = 1 
     GROUP BY a.iris_id;
     """
c.execute(r)

# final join and insert
print "final insert..."
r="""
INSERT INTO hlm( iris_id, nb_logement, nb_logement_hlm)
     SELECT tmp_nb.iris_id, tmp_nb.nb_logement, tmp_nb_hlm.nb_logement_hlm 
     FROM tmp_nb
     LEFT OUTER JOIN tmp_nb_hlm ON tmp_nb.iris_id==tmp_nb_hlm.iris_id
     ORDER BY tmp_nb.iris_id;
"""
c.execute(r)
 
# drop temp tables, closing
r="""DROP TABLE tmp_nb;"""
c.execute(r)
r="""DROP TABLE tmp_nb_hlm;"""
c.execute(r)

conn.commit()
conn.close()
