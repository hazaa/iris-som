# Avertissement

La base sqlite FD_LOGEMTZA_2012.db ci-dessus est __prête à l'emploi__. Elle se limite à la commune de Paris (champ COMMUNE='75056').

Elle contient les tables suivantes:

*  logement_2012: __avant agrégation__, 534727 lignes extraites de FD_LOGEMTZA_2012.txt avec uniquement les champs suivants:
    *   IRIS (integer): voir la description correspondante dans le documentation de l'insee.
    *   HLML (integer): idem
    *   AGEMEN8 (text): idem
    *   DIPLM (text): idem
    *   INP18N (text): idem
*  iris_unique_id: celle-ci contient la liste des IRIS __uniques__, i.e. sans répétition. 
*  hlm: __après agrégation__ au niveau de l'iris, celle-ci contient 943 lignes avec les champs suivants:
    *   iris_id (integer): code de l'iris 
    *   nb_logement (integer): nombre de logement échantillonné dans cet iris.
    *   nb_logement_hlm (integer): nombre de logement échantillonné dans cet iris qui sont aussi des logements hlm.
*  emplmZZ: __après agrégation__ au niveau de l'iris, celle-ci contient 943 lignes avec les champs suivants:
    *   iris_id (integer): code de l'iris 
    *   nb_logement (integer): nombre de logement échantillonné dans cet iris.
    *   nb_logement_emplmZZ (integer): nombre de logement échantillonné dans cet iris qui sont aussi tels que emplm='ZZ'.    
* avg_std:    __après agrégation__ au niveau de l'iris, celle-ci contient les champs suivants:
    *   iris_id (integer): code de l'iris 
    *   avg_agemen8 (real): moyenne par iris du champ agemen8 de la table logement_2012, calculé par la fonction [avg](http://sqlite.org/lang_aggfunc.html). 
    *   std_agemen8 (real): écart-type. sqlite3 ne dispose pas de cette fonction, on fait appel à un "user-defined aggregator", voir la fonction aggregate_RP.py 
    *   avg_inp18m (real): moyenne par iris du champ inp18m 
    *   std_inp18m (real): écart-type par iris du champ inp18m 
	*   avg_diplm (real): moyenne par iris du champ diplm recodé sur 4 modalités (voir aggregate_RP_diplm.py) en reprenant les groupes des recensements post 2012 (voir le champ diplm_15) 
    *   std_diplm (real): écart-type par iris du champ diplm recodé sur 4 modalités


Si vous souhaiter _reconstruire_ cette base à partir du fichier insee FD_LOGEMTZA_2012.txt, suivez les consignes ci-dessous.


# Recensement: créer une base sqlite à partir des fichiers txt
Les données du recensement sont au format texte ou dbase. Comme nous devrons souvent
effectuer des opérations de type [JOIN](https://fr.wikipedia.org/wiki/Jointure_%28informatique%29),
il est plus pratique de les migrer dans une base de données. Nous choisissons ici
sqlite, qui est plus facile à mettre en oeuvre.  

Après avoir téléchargé un fichier insee (par exemple FD_LOGEMTZA_2010.txt), on va créer une base de données sqlite à partir de ce fichier. Cela facilitera l'exploitation des données (par exemple dans R).
Depuis la ligne de commande linux, taper (vérifier les dépendances, ci-dessous):

    $sqlite3 FD_LOGEMTZA_2012.db

Ceci lance l'interface en ligne de commande dans laquelle nous tapons le code ci-dessous:      
    
    sqlite> .separator ";"
    sqlite> create table tmp (COMMUNE  text, ARM text, IRIS integer, ACHLR3 text, AEMM integer, AEMMR integer,  AGEMEN8 text, ANEM text, ANEMR text, ASCEN text, BAIN text,BATI text, CATIRIS text, CATL text, CHAU text, CHFL text, CHOS text, CLIM text,CMBL text, CUIS text, DEROU text, DIPLM text, EAU text, EGOUL text, ELEC text,EMPLM text, GARL text, HLML text,ILETUDM text, ILTM text, IMMIM text, INAIM text, INEEM text, INP11M text,INP16M text,INP18M text,INP19M text,INP24M text,INP3M text, INP60M text,INP65M text,INP6M text,INP75M text,INPAM text,INPER text,INPER1 text,INPER2 text,INPOM text,INPSM text,IPONDL text, MATRM text, METRODOM text,NBPI text,RECHM text, REGION integer, SANI text, SANIDOM text, SEXEM text, STOCD text, SURF text, TACTM text, TPM text, TRANSM text, TRIRIS text, TYPC text, TYPL text, VOIT text, WC text );
    sqlite> .import FD_LOGEMTZA_2012.txt tmp    
    
Importer tous les champs avec la commande sql [insert](https://fr.wikipedia.org/wiki/Insert_(SQL)) (attention, cela concerne toute l'Ile de France):

    sqlite> create table logement_2012 (id integer primary key,COMMUNE  text, ARM text, IRIS integer, ACHLR3 text, AEMM integer, AEMMR integer,  AGEMEN8 text, ANEM text, ANEMR text, ASCEN text, BAIN text,BATI text, CATIRIS text, CATL text, CHAU text, CHFL text, CHOS text, CLIM text,CMBL text, CUIS text, DEROU text, DIPLM text, EAU text, EGOUL text, ELEC text,EMPLM text, GARL text, HLML text,ILETUDM text, ILTM text, IMMIM text, INAIM text, INEEM text, INP11M text,INP16M text,INP18M text,INP19M text,INP24M text,INP3M text, INP60M text,INP65M text,INP6M text,INP75M text,INPAM text,INPER text,INPER1 text,INPER2 text,INPOM text,INPSM text,IPONDL text, MATRM text, METRODOM text,NBPI text,RECHM text, REGION integer, SANI text, SANIDOM text, SEXEM text, STOCD text, SURF text, TACTM text, TPM text, TRANSM text, TRIRIS text, TYPC text, TYPL text, VOIT text, WC text );
    sqlite> insert into logement_2012 (COMMUNE, ARM, IRIS, ACHLR3, AEMM, AEMMR,  AGEMEN8, ANEM, ANEMR, ASCEN, BAIN ,BATI , CATIRIS , CATL , CHAU , CHFL , CHOS , CLIM ,CMBL , CUIS , DEROU , DIPLM , EAU , EGOUL , ELEC ,EMPLM , GARL , HLML ,ILETUDM , ILTM , IMMIM , INAIM , INEEM , INP11M ,INP16M ,INP18M ,INP19M ,INP24M ,INP3M , INP60M ,INP65M ,INP6M ,INP75M ,INPAM ,INPER ,INPER1 ,INPER2 ,INPOM ,INPSM ,IPONDL , MATRM , METRODOM ,NBPI ,RECHM , REGION, SANI , SANIDOM , SEXEM , STOCD , SURF , TACTM , TPM , TRANSM , TRIRIS , TYPC , TYPL , VOIT , WC  ) select * from tmp;
    
Importer uniquement les champs IRIS et HLML :

    sqlite> create table logement_2012 (id integer primary key,IRIS integer,  HLML text);
    sqlite> insert into logement_2012 (IRIS, HLML) select IRIS,HLML from tmp;

OU Importer uniquement les champs IRIS et HLML de la commune de Paris:

    (sqlite> create table logement_2012 (id integer primary key,IRIS integer,  HLML text);)
    sqlite> create table logement_2012 (IRIS integer,  HLML integer);
    sqlite> insert into logement_2012 (IRIS, HLML) select IRIS,HLML from tmp where tmp.COMMUNE=='75056';

OU Importer uniquement les champs IRIS, HLML, AGEMEN8, DIPLM, INP24M de la commune de Paris:

    sqlite> create table logement_2012 (IRIS integer,  HLML integer, AGEMEN8 text, DIPLM text, INP24M text);
    sqlite> insert into logement_2012 (IRIS, HLML, AGEMEN8, DIPLM, INP24M) select IRIS,HLML,AGEMEN8,DIPLM,INP24M from tmp where tmp.COMMUNE=='75056';

OU Importer uniquement les champs IRIS, HLML, AGEMEN8, DIPLM, INP18M, EMPLM de la commune de Paris:

    sqlite> create table logement_2012 (IRIS integer,  HLML integer, AGEMEN8 text, DIPLM text, INP18M text, EMPLM text);
    sqlite> insert into logement_2012 (IRIS, HLML, AGEMEN8, DIPLM, INP18M,EMPLM) select IRIS,HLML,AGEMEN8,DIPLM,INP18M,EMPLM from tmp where tmp.COMMUNE=='75056';



Finalement on nettoie et on ferme:

    sqlite> drop table tmp; 
    sqlite> .q
    $sqlite3 FD_LOGEMTZA_2012.db "VACUUM;"

Inspecter le contenu de la base a posteriori:

	$sqlite3 FD_LOGEMTZA_2012.db
	sqlite> select * from logement_2012 WHERE rowid<10;
	sqlite> select count(*) from logement_2012 ; 

# Agréger les logements pour chaque iris

Utilisez pour cela les fichier python aggregate_RP_hlm.py, aggregate_RP_diplm.py et aggregate_RP.py situés dans le répertoire principal.


# Dépendances

*  paquet debian: sqlite
