#  distdist_compare.R
#  
#  Copyright 2017 Aurélien Hazan <>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

# SOMbrero: system dep r-cran-slam

setwd("~/local/git/iris-som")
library(rgdal)
library(RColorBrewer)
#library(spdep)
library(sp)
library(SOMbrero)
source('segregation_som.R')

###########################################################################################
#
#       Tests
#
############################################################################################

test.spatial.polygon.df.feitosa<-function(){
	lf = create.spatial.polygon.df.feitosa(2,n=12)
	spplot(lf$grd.df)
	}
test.synthetic.dataset.feitosa<-function(){
	create.synthetic.dataset.feitosa(2,n=12)	
}

test.spatial.polygon.df.feitosa.noise<-function(){	
	id=1
	l = create.spatial.polygon.df.feitosa(id,n=12,noise=10)	
	all(rowSums(attr(l$grd.df,'data'))==100)
	# spplot(l$grd.df)
}

###########################################################################################
#
#       Useful functions 
#
############################################################################################

create.synthetic.dataset.seg<-function(id){	
	# https://sites.google.com/site/hongseongyun/seg
		
	}


create.spatial.polygon.df.feitosa<-function(id,n=12,noise=0){	
	# see [Feitosa 2004] (cited by [Dabet,Floch])	
	grd <- GridTopology(cellcentre.offset=c(0.5,0.5),
                    cellsize=c(1,1), cells.dim=c(n,n))
	grd.sp <- as.SpatialPolygons.GridTopology(grd)
	segdata<-rep(0,4*n*n)
	dim(segdata)<-c(n*n,4)
	oracle_clust_id = rep(0, n*n)
	for (i in seq(n)){
		for (j in seq(n)){
				k = (i-1)*n +j
				if(id==1){					
					if((i<=(n/2)) & (j<=(n/2)) ){
						prob<-c(100,0,0,0)
						if(noise==0){segdata[k,]<-prob; } 					 
						else{#prob = prob + c(-noise,noise/3.,noise/3.,noise/3.)
							#segdata[k,] <-  rmultinom(1, 100, prob = prob)
							segdata[k,]<- prob + 25*noise*rnorm(4)
							}
						oracle_clust_id[k]=1 
						}
					if((i>(n/2)) & (j<=(n/2)) ){ 
						prob<-c(0,100,0,0)
						if(noise==0){segdata[k,]<-prob; } 					 
						else{#prob = prob + c(noise/3.,-noise,noise/3.,noise/3.)
							#segdata[k,] <-  rmultinom(1, 100, prob = prob)
							segdata[k,]<- prob + 25*noise*rnorm(4)
							}
						oracle_clust_id[k]=2
						}
					if((i<=(n/2)) & (j>(n/2)) ){
						prob<-c(0,0,100,0)
						if(noise==0){segdata[k,]<-prob; } 					 
						else{#prob = prob + c(noise/3.,noise/3.,-noise,noise/3.)
							#segdata[k,] <-  rmultinom(1, 100, prob = prob)
							segdata[k,]<- prob + 25*noise*rnorm(4)
							}
						oracle_clust_id[k]=3
						}
					if((i>(n/2)) & (j>(n/2)) ){
						prob<-c(0,0,0,100)
						if(noise==0){segdata[k,]<-prob; } 					 
						else{#prob = prob + c(noise/3.,noise/3.,noise/3.,-noise)
							#segdata[k,] <-  rmultinom(1, 100, prob = prob)
							segdata[k,]<- prob + 25*noise*rnorm(4)
							}
						oracle_clust_id[k]=4 
						 }
				 }
				 if(id==2){
					 if((i%%2==0) & (j%%2==0)){
						prob<-c(100,0,0,0)
						if(noise==0){segdata[k,]<-prob; } 					 
						else{#prob = prob + c(-noise,noise/3.,noise/3.,noise/3.)
							#segdata[k,] <-  rmultinom(1, 100, prob = prob)
							segdata[k,]<- prob + 25*noise*rnorm(4)
							}
						oracle_clust_id[k]=1 	 
						 }
					 if((i%%2==1) & (j%%2==0)){
						 prob<-c(0,100,0,0)
						if(noise==0){segdata[k,]<-prob; } 					 
						else{#prob = prob + c(noise/3.,-noise,noise/3.,noise/3.)
							#segdata[k,] <-  rmultinom(1, 100, prob = prob)
							segdata[k,]<- prob + 25*noise*rnorm(4)
							}
						oracle_clust_id[k]=2
						  }
					 if((i%%2==0) & (j%%2==1)){
						 prob<-c(0,0,100,0)
						if(noise==0){segdata[k,]<-prob; } 					 
						else{#prob = prob + c(noise/3.,noise/3.,-noise,noise/3.)
							#segdata[k,] <-  rmultinom(1, 100, prob = prob)
							segdata[k,]<- prob + 25*noise*rnorm(4)
							}
						oracle_clust_id[k]=3
						  }
					 if((i%%2==1) & (j%%2==1)){
						 prob<-c(0,0,0,100)
						if(noise==0){segdata[k,]<-prob; } 					 
						else{
							#prob = prob + c(noise/3.,noise/3.,noise/3.,-noise)
							#segdata[k,] <-  rmultinom(1, 100, prob = prob)
							segdata[k,]<- prob + 25*noise*rnorm(4)
							}
						oracle_clust_id[k]=4 
						 }
					 }
				 if(id==3){
					 prob=c(25,25,25,25)
					 if(noise==0){ 
						segdata[k,]<-prob}	
					 else{
						 # multinomial noise
						 #segdata[k,]<-  rmultinom(1, 100, prob = prob)
						 # normal noise
						 segdata[k,]<- prob + 25*noise*rnorm(4)
						 }
				 }
				 
		}
	}
	# normalization necessary. If not, segdata[k,] can be <0 or >100 because of noise
	if(noise>0){ 
		m= min(segdata); M=max(segdata)
		segdata<-100*(segdata-m)/(M-m)		
		}
	# create sp
	rownames(segdata) <- paste("g", 1:(n*n), sep = "")
	grd.df <- SpatialPolygonsDataFrame(grd.sp, data.frame(segdata))	
	list(grd=grd,grd.sp=grd.sp,grd.df=grd.df,oracle_clust_id= oracle_clust_id)
	}

create.synthetic.dataset.feitosa<-function(id,n=12,algo_cluster='oracle',noise=0){
	#
	l = create.spatial.polygon.df.feitosa(id,n=n,noise=noise)
	if(algo_cluster=='oracle'){
		d=data.frame(Cluster=l$grd.df, clust_id=l$oracle_clust_id)
		d['coords']= coordinates(l$grd.sp)
		# distance functions		
		#f1<-function(x,y){ abs(x$clust_id-y$clust_id)}
		f1<-function(x,y){ifelse(x==y,0,1)}  #Dirac
		f2<-function(x,y){ norm(as.matrix(x-y))}					
	}
	if(algo_cluster=='SOM'){
		m=8;n=8
		res.SOM <- trainSOM(x.data=attr(l$grd.df,'data'),dimension=c(m,n), type="numeric",
							scaling = "unitvar", maxit=10*dim(l$grd.df)[1])
		d=data.frame(Cluster=l$grd.df  )
		d['clust_id']= res.SOM$parameters$the.grid$coord[res.SOM$clustering,]
		d['coords']= coordinates(l$grd.sp)
		f1<-function(x,y){ norm(as.matrix(x-y),type='M') } #max
		f2<-function(x,y){ norm(as.matrix(x-y)) }					
		}
	if(algo_cluster=='kmeans'){}
	res<-list(d=d, f1=f1, f2=f2)
}


create.synthetic.dataset<-function(id="mixed_seg",algo_cluster='oracle'){	
	# CHANGE THIS DEPENDING ON YOUR DATASET	
	library(rgdal,quietly = TRUE)	
	# get coordinates
	d= "/home/aurelien/local/data/insee_ign_iris"  # change this	
	map <- readOGR(dsn=d, layer= "CONTOURS-IRIS75", verbose=FALSE)
	coords = coordinates(map)
	# matrix of proportions
	ngroups=4
	niris=dim(coords)[1]
	d<-local_count_synthetic(id, ngroups,niris)
	d['coords']= coords
	if(algo_cluster=='oracle'){
		# distance functions		
		f1<-function(x,y){ abs(x$clust_id-y$clust_id)}
		f2<-function(x,y){ norm(x$coords-y$coords) }
		# return
		#l<-list( data=lc$Cluster, var1=lc$clust_id , var2= coords)
		}
	if(algo_cluster=='SOM'){
		m=1;n=8
		res.SOM <- trainSOM(x.data=d[,1:4],dimension=c(m,n), type="numeric",
							scaling = "unitvar", maxit=10* dim(d)[1])
		d['clust_id']= res.SOM$parameters$the.grid$coord[res.SOM$clustering,]
		f1<-function(x,y){ norm(as.matrix(x-y),type='M') } #max
		f2<-function(x,y){ norm(as.matrix(x-y)) }					
		}
	if(algo_cluster=='kmeans'){}
			
	res<-list(d=d, f1=f1, f2=f2)
	}

dist_make <- function (x, distance_fcn, method=NULL) {
#https://rdrr.io/cran/usedist/src/R/distance_matrix.R
  distance_from_idxs <- function (idxs) {
    i1 <- idxs[1]
    i2 <- idxs[2]
    distance_fcn(x[i1,], x[i2,])
  }
  size <- nrow(x)
  d <- apply(utils::combn(size, 2), 2, distance_from_idxs)
  attr(d, "Size") <- size
  xnames <- rownames(x)
  if (!is.null(xnames)) {
    attr(d, "Labels") <- xnames
  }
  attr(d, "Diag") <- FALSE
  attr(d, "Upper") <- FALSE
  if (!is.null(method)) {
    attr(d, "method") <- method
  }
  class(d) <- "dist"
  d
}
	
plot.dist<-function(dist.1,dist.2){	
	# boxplot
	dist.1 <- as.vector(dist.1)
	dist.2 <- as.vector(dist.2)	
	fa=factor(dist.1)
	d = data.frame( d2=dist.2, d1=fa )	 
	boxplot(d2~d1,data = d ) 
	# histogram
	}
	
###########################################################################################
#
#       Main
#
############################################################################################

main<-function(){
	#################################
	# DATA = synth ; CLUSTER= oracle ; DIST_2 = ficelle
	
	# load dataset
	name =  "random" #"mixed_seg","unif_seg_1","unif_seg_2","random"
	#fname = paste('fig/distdist_box_synth_',name,'.pdf',sep='')
	fname = paste('fig/distdist_box_synth_som_',name,'.pdf',sep='')
	l<-create.synthetic.dataset(name,algo_cluster="SOM") 
	
	# compute distance matrices
	dist.mat1<-dist(l$d$clust_id)
	dist.mat2<-dist(l$d$coords)
	
	# plot & write to file
	pdf(fname)
	plot.dist(dist.mat1,dist.mat2)
	dev.off()
	
	#################################
	# DATA = feitosa ; CLUSTER= oracle ; DIST_2 = dirac(c1-c2)
	# load dataset
	name =  "3" 
	fname = paste('fig/distdist_box_feitosa_',name,'.pdf',sep='')
	l<-create.synthetic.dataset.feitosa(id=strtoi(name),n=12,algo_cluster='oracle')
	
	# compute distance matrices
	dist.mat1<-dist_make(as.matrix(l$d$clust_id),l$f1)
	dist.mat2<-dist_make(as.matrix(l$d$coords),l$f2)
	
	# plot & write to file
	pdf(fname)
	plot.dist(dist.mat1,dist.mat2)
	dev.off()
	
	#################################
	# DATA = feitosa  NO NOISE; CLUSTER= som ; DIST_2 = som
	
	name =  "3" # "1", "2" , "3" , 
	fname = paste('fig/distdist_box_feitosa_som',name,'.pdf',sep='')
	l<-create.synthetic.dataset.feitosa(id=strtoi(name),n=12,algo_cluster='SOM')
	
	# compute distance matrices
	dist.mat1<-dist_make(as.matrix(l$d$clust_id),l$f1)
	dist.mat2<-dist_make(as.matrix(l$d$coords),l$f2)
	
	# plot & write to file
	pdf(fname)
	plot.dist(dist.mat1,dist.mat2)
	dev.off()
	
	#################################
	# DATA = feitosa WITH NOISE; CLUSTER= som ; DIST_2 = som
	
	noise = 2.0 #0.01 0.05 0.1 2.0
	name =  "1" # "1", "2" , "3" , 
	fname = paste('fig/distdist_box_feitosa_som',name,'_noise',noise,'.pdf',sep='')
	l<-create.synthetic.dataset.feitosa(id=strtoi(name),n=12,algo_cluster='SOM',
										noise =noise)
	
	# compute distance matrices
	dist.mat1<-dist_make(as.matrix(l$d$clust_id),l$f1)
	dist.mat2<-dist_make(as.matrix(l$d$coords),l$f2)
	
	# plot & write to file
	pdf(fname)
	plot.dist(dist.mat1,dist.mat2)
	dev.off()
	
	# plot the grid
	m=8;n=8
	l = create.spatial.polygon.df.feitosa(strtoi(name),n=12,noise=noise)
	res.SOM <- trainSOM(x.data=attr(l$grd.df,'data'),dimension=c(m,n), type="numeric",
						scaling = "unitvar", maxit=10*dim(l$grd.df)[1])
	fname = paste('fig/grid_feitosa_som',name,'_noise',noise,'.pdf',sep='')			
	pdf(fname)
	plot(res.SOM)
	dev.off()
	fname = paste('fig/grid_feitosa_som',name,'_noise',noise,'.png',sep='')			
	png(fname)
	plot(res.SOM)
	dev.off()
	#################################
	# plot local count feitosa NO NOISE
	name = "3"
	fname = paste('fig/lc_feitosa_',name,'.pdf',sep='')
	lf = create.spatial.polygon.df.feitosa(strtoi(name),n=12)
	pdf(fname)
	spplot(lf$grd.df)
	dev.off()
	
	#################################
	# plot local count feitosa WITH NOISE
	name = "1"
	noise = 2.0 #0.01 0.05 0.1 1.0 2.0
	fname = paste('fig/lc_feitosa_',name,'_noise',noise,'.pdf',sep='')
	lf = create.spatial.polygon.df.feitosa(strtoi(name),n=12,noise=noise)
	pdf(fname)
	spplot(lf$grd.df)
	dev.off()
	
}
