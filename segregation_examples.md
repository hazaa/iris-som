# Segregation: exemples sur données synthétiques à deux classes


Le code pour générer ces exemples utilise les exemples segdata du package R seg, 
et se trouve dans le fichier: [segregation_examples.R](segregation_examples.R) 


Les paramètres employés: smoothing = "kernel",nrow = 10, ncol = 10


| nom |H  | H_smooth | carte des clusters (no smooth)  |  carte (avec smooth) | 
| -------- | -------- | -------- | -------- | -------- |
| segdata_1 | 0.79 | 0.69 | ![segdata_1](fig/segdata_1.png) | ![segdata_1](fig/segdata_1_smooth.png)|
| segdata_2 |  0.47 | 0.27 | ![segdata_2](fig/segdata_2.png) | ![segdata_2](fig/segdata_2_smooth.png)|
| segdata_3 | 0.17 | 0.00 | ![segdata_3](fig/segdata_3.png) | ![segdata_3](fig/segdata_3_smooth.png)|
| segdata_4 |  0.57 | 0.50 | ![segdata_4](fig/segdata_4.png) | ![segdata_4](fig/segdata_4_smooth.png)|
| segdata_5 | 0.33 | 0.18 | ![segdata_5](fig/segdata_5.png) | ![segdata_5](fig/segdata_5_smooth.png)|
| segdata_6 | 0.78 | 0.68 | ![segdata_6](fig/segdata_6.png) | ![segdata_6](fig/segdata_6_smooth.png)|
| segdata_7 | 0.58 | 0.43 | ![segdata_7](fig/segdata_7.png) | ![segdata_7](fig/segdata_7_smooth.png)|
| segdata_8 | 0.50 | 0.35 | ![segdata_8](fig/segdata_8.png) | ![segdata_8](fig/segdata_8_smooth.png)|
