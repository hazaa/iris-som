#!/usr/bin/python
# -*- coding: utf-8 -*-
# this script is taken from: http://stackoverflow.com/questions/2298339/standard-deviation-for-sqlite

#Values produced by this script can be verified by follwing the steps
#found at https://support.microsoft.com/en-us/kb/213930 to Verify
#by chosing a non memory based database.

import sqlite3
import math
import random
import os
import sys
import traceback
import random

"""
class StdevFunc:
    def __init__(self):
        self.M = 0.0    #Mean
        self.V = 0.0    #Used to Calculate Variance
        self.S = 0.0    #Standard Deviation
        self.k = 1      #Population or Small 

    def step(self, value):
        try:
            if value is None:
                return None

            tM = self.M
            self.M += (value - tM) / self.k
            self.V += (value - tM) * (value - self.M)
            self.k += 1
        except Exception as EXStep:
			pass
			return None    

	def finalize(self):		
		try:
			if ((self.k - 1) < 3):
				return None			
            #Now with our range Calculated, and Multiplied finish the Variance Calculation
			self.V = (self.V / (self.k-2))
            #Standard Deviation is the Square Root of Variance
			self.S = math.sqrt(self.V)
			return self.S
		except Exception as EXFinal:
			pass
			return None 
"""



class StdevFunc0:
    """
    For use as an aggregate function in SQLite
    """
    def __init__(self):
        self.M = 0.0
        self.S = 0.0
        self.k = 0

    def step(self, value):
        try:
            # automatically convert text to float, like the rest of SQLite
            val = float(value) # if fails, skips this iteration, which also ignores nulls
            tM = self.M
            self.k += 1
            self.M += ((val - tM) / self.k)
            self.S += ((val - tM) * (val - self.M))
        except:
            pass

    def finalize(self):
        if self.k <= 1: # avoid division by zero
            return none
        else:
            return math.sqrt(self.S / (self.k-1))

if __name__ == "__main__":
	with sqlite3.connect(':memory:') as con:

		con.create_aggregate("stdev", 1, StdevFunc0)

		cur = con.cursor()

		cur.execute("create table test(i)")
		cur.executemany("insert into test(i) values (?)", [(1,), (2,), (3,), (4,), (5,)])
		cur.execute("insert into test(i) values (null)")
		cur.execute("select avg(i) from test")
		print("avg: %f" % cur.fetchone()[0])
		cur.execute("select stdev(i) from test")
		print("stdev: %f" % cur.fetchone()[0])
