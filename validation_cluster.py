library(factoextra)
library(readxl)
library(cluster)
library(SOMbrero) 
library(FactoMineR)
# ----------------------
# LOAD DATA
do_merge=FALSE
data=0
if(do_merge){
	# SOM1+2+3
	data.iris.revenu <- read_excel(path="data/variables_IRIS.xlsx")
	d1=data.iris.revenu[,c(1,5:9)]
	# SOM2 (variables sur les caractéristiques de la population)
	data.iris.population <- read_excel(path="data/variables_SOM2_943.xlsx")
	d2=data.iris.population[,c(1,5:7,9,10)]
	# SOM3 (variables sur les services)
	data.iris.services <- read_excel(path="data/variables_SOM3_980.xlsx")
	d3=data.iris.services[,c(1,5:15, 19:21, 28)]
	# merge
	d12 <- merge(d1, d2, by="IRIS" ,all=FALSE)
	d123 <- merge(d12, d3, by="IRIS" ,all=FALSE)
	rownames(d123)<-d123$IRIS
	data<-d123[,2:26]
}
else
{   # SOM1 (variables de revenu)
	data.iris.revenu <- read_excel(path="data/variables_IRIS.xlsx")
	d1=data.iris.revenu[,c(1,5:9)]
	rownames(d1)<-d1$IRIS
	data<-d1[,2:6]
	}

# -------------------------
# CLUSTER VALIDATION
#   comparer clustering avec pls méthodes (SOM+Hclust, K-means+Hclust, ACP+Hclust, Hclust)
#   et selon pls metriques (variance intra,...) 
#   lorsque K varie.
# 
# [NCAA paper]
# §3: "and then to classify individual IRIS blocks into four groups for sets 
#   1 and 2 and six groups for the third set."
# 
# Some references: Hastie et al. "The elements..."
#    14.3.5 Combinatorial Algorithms  "between-cluster", "within cluster"
#    14.3.11 Practical Issues "gap statistic"
#            "Thus cross-validation techniques, so useful for model selection in supervised learning, cannot be utilized in this context."
#    14.3.12 Hierarchical Clustering



fname = "data/cluster_validation.RData"
read_from_file=TRUE

# define clustering functions
pam1 <- function(x,k) list(cluster = pam(x,k, cluster.only=TRUE))
hclusCut <- function(x, k, d.meth = "euclidean", ...)
	list(cluster = cutree(hclust(dist(x, method=d.meth), ...), k=k))
pca_hclus<- function(x, k, d.meth = "euclidean"){ 
	res.pca <- PCA(x, ncp = 2, graph = FALSE)
	coord=res.pca$ind$coord
	list(cluster = cutree(hclust(dist(coord, method=d.meth)), k=k))
	#res.hcpc <- HCPC(res.pca, nb.clust=k, graph = FALSE)
	#list(cluster=res.hcpc$data.clust)
	}
som_hclus<- function(x, k,m=8,n=8){ 
	dim.donnees <- dim(x)[1]
	res.SOM <- trainSOM(x.data=x, dimension=c(m,n), type="numeric",scaling = "unitvar", maxit=10*dim.donnees)
	res.SOM.sc <- superClass(res.SOM, k=k)
	list(cluster=res.SOM.sc$cluster[res.SOM$clustering])
}
# compute gap statistics, etc...
# function: clusGap
# package: https://cran.r-project.org/web/packages/cluster/
# ref: Tibshirani, R., Walther, G. and Hastie, T. (2001). Estimating the number of data clusters via the
#      Gap statistic. Journal of the Royal Statistical Society B, 63, 411–423.
if(read_from_file){
	load( fname)
	}
else{	
	gs.kmn <- clusGap(data, FUN = kmeans, nstart = 20, K.max = 8, B = 60)
	gs.pam <- clusGap(data, FUN = pam1, K.max = 8, B = 60)
	gs.hclus <- clusGap(data, FUN = hclusCut, K.max = 8, B = 60)
	#gs.pcahclus <- clusGap(data, FUN = pca_hclus, K.max = 8, B = 60)
	gs.pcahclus <- clusGap(data, FUN = pca_hclus, K.max = 8, B = 60)
	gs.som <- clusGap(data, FUN = som_hclus, K.max = 8, B = 60)
	save(gs.kmn, gs.pam, gs.hclus,gs.som ,gs.pcahclus , file = fname)
}
# within-cluster variance across algorithms
algo_names<-c('kmn', 'pam', 'hclust','som+hclust','pca+hclust')	
varname= "logW"#"E.logW"
xrange=c(1,8)
yrange=range(gs.som$Tab[, varname],range(gs.kmn$Tab[, varname]))
plot(xrange, yrange, type="n", xlab="nb clusters",
   ylab=varname ) 
lines(gs.kmn$Tab[, varname], lty=1)
lines(gs.pam$Tab[, varname], lty=2)
lines(gs.hclus$Tab[, varname], lty=3)
lines(gs.som$Tab[, varname], lty=4)
lines(gs.pcahclus$Tab[, varname], lty=5)
legend("topright",algo_names,lty=c(1,2,3,4,5))

# plot gap stats
par(mfrow=c(2,2)) 
fviz_gap_stat(gs.kmn,maxSE = list(method ="firstSEmax", SE.factor = 1))
fviz_gap_stat(gs.pam,maxSE = list(method ="firstSEmax", SE.factor = 1))
fviz_gap_stat(gs.hclus,maxSE = list(method ="firstSEmax", SE.factor = 1))
fviz_gap_stat(gs.som,maxSE = list(method ="firstSEmax", SE.factor = 1))
fviz_gap_stat(gs.pcahclus,maxSE = list(method ="firstSEmax", SE.factor = 1))

# compute optimal k 
SE.factor=1
mets <- formals(maxSE)$method
l=list("firstSEmax", "Tibs2001SEmax", "globalSEmax", "firstmax", "globalmax")
K.min = 2
r=sapply(list(gs.kmn, gs.pam, gs.hclus,gs.som,gs.pcahclus), function(gs)
	lapply(l, function(M){ 	
		K.max=gs$call$K.max		
		gap <- gs$Tab[K.min:K.max, "gap"]
		se <- gs$Tab[K.min:K.max, "SE.sim"]
		#print(list(gap, se, M, SE.factor))
		k <- maxSE(gap, se, method = M, SE.factor = SE.factor)
		return(k)		
		}))
	
colnames(r)<-algo_names
rownames(r)<-l
print(r)


#-----------------
# Bayesian metric fo optimal nb of cluster
# 'the optimal model and number of clusters according to the Bayesian Information Criterion for expectation-maximization, initialized by hierarchical clustering for parameterized Gaussian mixture models'
# See http://www.jstatsoft.org/v18/i06/paper
# http://www.stat.washington.edu/research/reports/2006/tr504.pdf

library(mclust)
d_clust <- Mclust(as.matrix(data), G=1:20)
m.best <- dim(d_clust$z)[2]
cat("model-based optimal number of clusters:", m.best, "\n")
# plot
par(mfrow=c(2,1))
plot(d_clust,what='BIC')
plot(d_clust,what='classification')

#-----------------
# optimum average silhouette width
library(fpc)
pamk.best <- pamk(data)
cat("number of clusters estimated by optimum average silhouette width:", pamk.best$nc, "\n")
plot(pam(data, pamk.best$nc))

#-----------------------
# Heuristique Delta(Q)/Delta(Q+1)
# REF: Husson, Josse, Pagès 2010 "Principal component methods - hierarchical clustering - partitional clustering: why would we need to choose for visualizing data?"

varname= "logW"
r=sapply(list(gs.kmn, gs.pam, gs.hclus,gs.som,gs.pcahclus), function(gs){
	K.max=gs$call$K.max
	i=which.min(gs.kmn$Tab[1:K.max-1, varname]/gs.kmn$Tab[2:K.max, varname])		
	return(i+1)
	})
r=as.matrix(r)	
rownames(r)<-algo_names
print(r)
# ----------------------
# Exemple détaillé PCA + Hclust

# REF: Husson, Josse, Pagès 2010 "Principal component methods - hierarchical clustering - partitional clustering: why would we need to choose for visualizing data?"
res.pca <- PCA(data, ncp = 5, graph = FALSE)
# Compute hierarchical clustering on principal components
res.hcpc <- HCPC(res.pca, graph = FALSE,min=2, max=10)

# cluster assignments
head(res.hcpc$data.clust, 10)
# quantitative variables that describe the most each cluster:
res.hcpc$desc.var$quanti
# principal dimensions that are the most associated with clusters:
res.hcpc$desc.axes$quanti

fviz_cluster(res.hcpc,
             repel = FALSE,            # Avoid label overlapping
             show.clust.cent = TRUE, # Show cluster centers
             palette = "jco",         # Color palette see ?ggpubr::ggpar
             ggtheme = theme_minimal(),
             main = "Factor map"
             )        
                  
# the within-group inertia of the partitioning into Q clusters
res.hcpc$call$t$intra
#the increase in between-group inertia (or equivalently the 
#decrease in within-group inertia) when moving from Q to Q + 1 clusters)
res.hcpc$call$t$inert.gain
# NB of CLUSTERS
res.hcpc$call$t$nb.clust
#"the ratio between two successive within-group inertias is given in $quot"
#"[it is] such that the criterion $call$t$quot might be as small as possible"
plot(res.hcpc$call$t$quot)

#-----------------
# OTHER packages
# https://cran.r-project.org/web/packages/clusteval/
# https://cran.r-project.org/web/packages/clValid
# https://cran.r-project.org/web/packages/optCluster
library(clValid)
intern <- clValid(as.matrix(data), 2:6, clMethods=c("hierarchical","kmeans","pam"),
			validation="internal")
summary(intern)
